﻿using AutoMapper;
using Hms.Web.ViewModels;
using HMS.Model;

namespace Hms.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Doctor, DoctorViewModel>();
            CreateMap<Patient, PatientViewModel>();
            CreateMap<Room, RoomViewModel>();
            CreateMap<Bill, BillViewModel>();
            CreateMap<Department, DepartmentViewModel>();
            CreateMap<AdminUser, AdminUserViewModel>();
            CreateMap<AllUsers,AllUsersViewModel>();
        }

        public override string ProfileName => "DomainToViewModelMappings";
    }
}