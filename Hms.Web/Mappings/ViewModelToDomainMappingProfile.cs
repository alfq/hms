using System;
using System.Linq;
using AutoMapper;
using Hms.Web.ViewModels;
using HMS.Data;
using HMS.Model;
using Microsoft.AspNet.Identity;
namespace Hms.Web.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        private readonly PasswordHasher _passwordHasher = new PasswordHasher();
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<DoctorViewModel, Doctor>()
                .ForMember(d => d.PasswordHash, a => a.ResolveUsing
                    (x => _passwordHasher.HashPassword(x.Password)))
                .ForMember(x => x.UserName, x => x.MapFrom(c => c.Email))
                .ForMember(x => x.SecurityStamp, a => a.ResolveUsing(x => Guid.NewGuid().ToString("D")));
            CreateMap<PatientViewModel, Patient>()
                .ForMember(x => x.PasswordHash, a => a.ResolveUsing
                    (x => _passwordHasher.HashPassword(x.Password)))
                .ForMember(x => x.UserName, b => b.MapFrom(c => c.Email))
                .ForMember(x => x.SecurityStamp, a => a.ResolveUsing(x => Guid.NewGuid().ToString("D")));
            CreateMap<BillViewModel, Bill>();
            CreateMap<DepartmentViewModel, Department>();
            CreateMap<RoomViewModel, Room>();
            CreateMap<AdminUserViewModel, AdminUser>()
                .ForMember(x => x.Id, c => c.Ignore())
                .ForMember(x => x.PasswordHash, a => a.ResolveUsing
                    (x => _passwordHasher.HashPassword(x.Password)))
                .ForMember(x => x.UserName, b => b.MapFrom(c => c.Email))
                .ForMember(x => x.SecurityStamp, a => a.ResolveUsing(x => Guid.NewGuid().ToString("D")));
            CreateMap<AllUsersViewModel, AllUsers>();
        }

        public override string ProfileName => "ViewModelToDomainMappings";
    }

}