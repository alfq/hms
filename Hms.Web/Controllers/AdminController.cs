﻿using System.Linq;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Hms.Web.ViewModels;
using HMS.Model;
using HMS.Service;
using Microsoft.AspNet.Identity;

namespace Hms.Web.Controllers
{
    [RoutePrefix("api/admin")]
    public class AdminController : BaseApiController
    {
        private readonly IAdminUserService adminUserService;
         

        public AdminController(IAdminUserService adminUserService)
        {
            this.adminUserService = adminUserService;
        }

        public AdminController()
        {
            
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult CreateAdmin(AdminUserViewModel viewModel)
        {
            var admin = Mapper.Map<AdminUserViewModel, AdminUser>(viewModel);
            adminUserService.Create(admin);
            adminUserService.Save();
            return Ok(admin);
            
        }

        [Authorize]
        [HttpGet]
        [Route("get")]
        public IHttpActionResult GetAdmin()
        {
            var name = User.Identity.GetUserName();
            var user = adminUserService.GetAdminUsers().Where(a=>a.UserName == name);
            return Ok(user);
        }

        [Authorize]
        [Route("getall")]
        public IHttpActionResult GetAll()
        {
            var admins = adminUserService.GetAdminUsers().ToList();
            return Ok(admins);
        }
    }
}