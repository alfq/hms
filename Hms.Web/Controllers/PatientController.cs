﻿using System.Linq;
using System.Web.Http;
using AutoMapper;
using Hms.Web.ViewModels;
using HMS.Model;
using HMS.Service;
using Microsoft.AspNet.Identity;

namespace Hms.Web.Controllers
{
    [RoutePrefix("api/patients")]
    public class PatientController : BaseApiController
    {
        private IPatientService patientService;
        private readonly UserManager<AllUsers> _userManager;

        public PatientController(IPatientService patientService, UserManager<AllUsers> userManager)
        {
            this.patientService = patientService;
            _userManager = userManager;
        }

        public PatientController()
        {
            
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult CreatePatient(PatientViewModel viewModel)
        {
            var patient = Mapper.Map<PatientViewModel, Patient>(viewModel);
            patientService.CreatePatient(patient);
            patientService.SavePatient();
            _userManager.AddToRole(patient.Id, "Patient");
            patientService.SavePatient();
            return Ok(patient);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("get/{id}")]
        public IHttpActionResult GetPatient(string id)
        {
            var patient = patientService.GetPatients().Where(p => p.Id == id);
            return Ok(patient);
        }

        [Authorize]
        [Route("getall")]
        public IHttpActionResult GetPatients()
        {
            var patients = patientService.GetPatients().ToList();
            return Ok(patients);
        }
    }
}