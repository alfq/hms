﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Hms.Web.ViewModels;
using HMS.Model;
using HMS.Service;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Hms.Web.Controllers
{
    [RoutePrefix("api/doctors")]
    public class DoctorsController : BaseApiController
    {
        
        private IDoctorService doctorService;
        private SignInManager<AllUsers,string> _signInManager;
        private UserManager<AllUsers> _userManager;
        private IAuthenticationManager _authenticationManager;

        public DoctorsController(IDoctorService doctorService, UserManager<AllUsers> userManager,
            SignInManager<AllUsers, string> signInManager, IAuthenticationManager authenticationManager)
        {
            this.doctorService = doctorService;
            _userManager = userManager;
            _signInManager = signInManager;
            _authenticationManager = authenticationManager;
        }

        public DoctorsController()
        {
            
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult CreateDoctor(DoctorViewModel viewModel)
        {
            var doc = Mapper.Map<DoctorViewModel,Doctor>(viewModel);
            doctorService.CreateDoctor(doc);
            doctorService.SaveDoctor();
            _userManager.AddToRoles(doc.Id, "Doctor");
            doctorService.SaveDoctor();
            return Ok(doc);
        }

        //[Authorize]
        [HttpGet]
        [Route("Get")]
        public IHttpActionResult GetDoctors()
        {
            var docs = doctorService.GetDoctors();
            return Ok(docs);
        }

        [HttpPost]
        [Route("signindoc")]
        public async Task<IHttpActionResult> SignInDoc(DoctorViewModel viewModel)
        {
            var doc = doctorService.GetDoctor(viewModel.Email) as AllUsers;
           // var user = _userManager.FindByEmailAsync(viewModel.Email);
            var identity = await _userManager.CreateIdentityAsync(doc, DefaultAuthenticationTypes.ExternalCookie);
            _authenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, identity);
            //return Ok("Signed in");
           return Redirect("http://localhost:55449/api/doctors/getbyid/"+doc.Id);
        }

        [HttpGet]
        [Route("getbyid/{id}")]
        public async Task<IHttpActionResult> GetById([FromUri]string id)
        {
            var doc = doctorService.GetDoctorById(id);
            var docAsAllUsers = doc as AllUsers;
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = _userManager.CreateIdentity(docAsAllUsers,DefaultAuthenticationTypes.ExternalCookie);
            _authenticationManager.SignIn(new AuthenticationProperties{IsPersistent = false},identity);
            return Ok(docAsAllUsers);
        }
    }
}