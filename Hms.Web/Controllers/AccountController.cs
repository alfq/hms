﻿using System;
using System.Web.Http;
using AutoMapper;
using Hms.Web.ViewModels;
using HMS.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.Cookies;

namespace Hms.Web.Controllers
{
    [RoutePrefix("api/account")]
    public class AccountController : BaseApiController
    {
        private SignInManager<AllUsers, string> _signInManager;
        private UserManager<AllUsers> _userManager;

        public AccountController(SignInManager<AllUsers, string> signInManager, UserManager<AllUsers> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [HttpPost]
        [Route("signin")]
        public IHttpActionResult SignIn(AllUsersViewModel viewModel)
        {
            //var allUsers = Mapper.Map<AllUsersViewModel,AllUsers>(viewModel);
            var getUser = _userManager.FindByEmail(viewModel.Email);
            if (getUser == null) return InternalServerError(new Exception("User not found"));
            _signInManager.SignIn(getUser, true, false);
            return Ok("Signed in");
            // return Redirect("http://localhost:55449/api/doctors/get");
        }

        [HttpPost]
        [Route("signout")]
        public IHttpActionResult SignOut()
        {
            _signInManager.AuthenticationManager.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok("Signed out");
        }
    }

}