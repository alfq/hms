﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using Hms.Web.ViewModels;
using HMS.Model;
using HMS.Service;

namespace Hms.Web.Controllers
{
    [RoutePrefix("api/rooms")]
    public class RoomController : BaseApiController
    {
        private readonly IRoomService _roomService;

        public RoomController(IRoomService roomService)
        {
            _roomService = roomService;
        }

        public RoomController()
        {
            
        }

        [HttpPost]
        [Route("create")]
        [ResponseType(typeof(void))]
        public IHttpActionResult Create(RoomViewModel viewModel)
        {
            if (!ModelState.IsValid || viewModel == null)
            {
                return
                    ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "The room details entered isn't valid"));
            }
            var room = Mapper.Map<RoomViewModel,Room>(viewModel);
            _roomService.Create(room);
            _roomService.Save();
            return Ok(room);
        }

        [HttpDelete]
        [Route("delete/{name}")]
        public IHttpActionResult Delete([FromUri]string name)
        {
            if (!ModelState.IsValid || string.IsNullOrEmpty(name))
            {
                return
                    ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "The room details entered isn't valid"));
            }
            _roomService.DeleteByName(name);
            _roomService.Save();
            return Ok("Deleted");
        }

        [HttpGet]
        [Route("getroom/{name}")]
        public IHttpActionResult GetRoom([FromUri]string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return
                    ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Room name can not be empty"));
            }
            var room  = _roomService.GetRoomByName(name);
            if (room != null)
            {
                return Ok(room);
            }
            return
                ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "The specified room was not found"));
        }

        [HttpGet]
        [Route("getallrooms")]
        public IHttpActionResult GetRooms()
        {
            var rooms = _roomService.GetRooms().ToList();
            if (!rooms.Any())
            {
                return ResponseMessage(Request.CreateResponse("No rooms found"));
            }
            return Ok(rooms);
        }
    }
}