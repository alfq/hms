﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Hms.Web.ViewModels;
using HMS.Model;
using HMS.Service;

namespace Hms.Web.Controllers
{
    [RoutePrefix("api/departments")]
    public class DepartmentController : BaseApiController
    {

        private readonly IDepartmentService _departmentService;
        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        public DepartmentController()
        {
            
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(DepartmentViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return
                    ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "The department details entered isn't valid"));
            }
            var department = Mapper.Map<DepartmentViewModel, Department>(viewModel);
            _departmentService.Create(department);
            _departmentService.Save();
            return Ok(department);
        }

        [HttpDelete]
        [Route("delete/{name}")]
        public IHttpActionResult Delete([FromUri]string name)
        {
            if (!ModelState.IsValid || string.IsNullOrEmpty(name))
            {
                return
                    ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "The department details entered isn't valid"));
            }
            _departmentService.DeleteDepartmentByName(name);
            _departmentService.Save();
            return Ok("Deleted");
        }

        [HttpGet]
        [Route("getroom/{name}")]
        public IHttpActionResult GetDepartment([FromUri]string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return
                    ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Department name can not be empty"));
            }
            var department = _departmentService.GetDepartmentByName(name);
            if (department != null)
            {
                return Ok(department);
            }
            return
                ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.NotFound, "The specified department was not found"));
        }

        [HttpGet]
        [Route("getalldepts")]
        public IHttpActionResult GetDepartments()
        {
            var departments = _departmentService.GetDepartments().ToList();
            if (!departments.Any())
            {
                return ResponseMessage(Request.CreateResponse("No departments found"));
            }
            return Ok(departments);
        }
    }
}
