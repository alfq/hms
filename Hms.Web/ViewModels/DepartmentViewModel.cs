﻿using System.Collections.ObjectModel;
using HMS.Model;

namespace Hms.Web.ViewModels
{
    public class DepartmentViewModel
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        //public Collection<Doctor> Doctors { get; set; }
       // public string DoctorId { get; set; }
    }
}