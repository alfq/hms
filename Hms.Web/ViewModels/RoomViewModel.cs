using HMS.Model;

namespace Hms.Web.ViewModels
{
    public class RoomViewModel
    {
        public int RoomId { get; set; }
        public string RoomNumber { get; set; }
        public RoomType RoomType { get; set; }
    }
}