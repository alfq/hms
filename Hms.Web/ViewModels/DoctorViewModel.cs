﻿using System.Collections.Generic;
using HMS.Model;

namespace Hms.Web.ViewModels
{
    public class DoctorViewModel
    {
        public int DoctorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => FirstName + " " + LastName;
        public string Address { get; set; }
        public Gender Gender { get; set; }
        public string PhoneNumber { get; set; }
        //public Department Department { get; set; }
        public int DepartmentId { get; set; }
        public List<Patient> Patients { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}