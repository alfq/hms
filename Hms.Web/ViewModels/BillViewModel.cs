using System;
using HMS.Model;

namespace Hms.Web.ViewModels
{
    public class BillViewModel
    {
        public int BillId { get; set; }
        public decimal DoctorBillCharge { get; set; }
        public decimal RoomBillCharge { get; set; }
        public int PatientId { get; set; }
        public DateTime DateOfAdmission { get; set; }
        public DateTime DateOfDischarge { get; set; }
    }
}