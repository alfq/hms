using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using Newtonsoft.Json.Serialization;

namespace Hms.Web.Helpers
{
    public class JsonFormat
    {
        public static void JsonFormatter(HttpConfiguration httpConfig)
        {
            httpConfig.MapHttpAttributeRoutes();
            var jsonFormatter = httpConfig.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}