﻿using System;
using System.Configuration;
using System.Web.Http;
using Autofac;
using Hms.Web.Helpers;
using Hms.Web.Infrastructure;
using HMS.Data;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Owin;

namespace Hms.Web
{
    public partial class Startup
    {
        private static void ConfigureOAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(() => new HmsEntities());
            app.CreatePerOwinContext<UserManager<IdentityUser>>(CreateManager);

            app.CreatePerOwinContext<AppRoleManager>(AppRoleManager.Create);

            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions()
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new CustomAuthorizationServerProvider(),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                AllowInsecureHttp = true,
                AccessTokenFormat = new CustomJwtFormat("http://localhost:55449")
            });
        }

        private static IContainer ContainerConfig(HttpConfiguration httpConfig)
        {
            return Bootstrapper.SetAutoFacContainer(httpConfig);
        }

        private static UserManager<IdentityUser> CreateManager(
            IdentityFactoryOptions<UserManager<IdentityUser>> options, IOwinContext context)
        {
            var appDbContext = context.Get<HmsEntities>();
            var appUserManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(appDbContext));
            return appUserManager;
        }

        private static void ConfigureOAuthTokenConsumption(IAppBuilder app)
        {
            const string issuer = "http://localhost:55449";
            var audienceId = ConfigurationManager.AppSettings["as:AudienceId"];
            var audienceSecret = TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["as:AudienceSecret"]);

            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] {audienceId},
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                {
                    new SymmetricKeyIssuerSecurityTokenProvider(issuer, audienceSecret),
                }
            });
        }
    }
}