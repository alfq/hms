﻿using System.Web.Http;
using Hms.Web.Helpers;
using Hms.Web.Mappings;
using Microsoft.Owin.Cors;
using Owin;

namespace Hms.Web
{
    public partial class Startup
    {
        private readonly HttpConfiguration _httpConfiguration;

        public Startup()
        {
            _httpConfiguration = new HttpConfiguration();
        }

        public void Configuration(IAppBuilder app)
        {
            JsonFormat.JsonFormatter(_httpConfiguration);

            ConfigureOAuth(app);
            ConfigureOAuthTokenConsumption(app);
            var container = ContainerConfig(_httpConfiguration);
            app.UseAutofacMiddleware(container);

            AutoMapperConfiguration.Configure();

            app.UseCors(CorsOptions.AllowAll);
            app.UseWebApi(_httpConfiguration);

        }
    }

}