﻿using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Hms.Web.Controllers;
using Hms.Web.Infrastructure;
using Hms.Web.Mappings;
using HMS.Data;
using HMS.Data.Infrastructure;
using HMS.Data.Repositories;
using HMS.Model;
using HMS.Service;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Hms.Web
{
    public class Bootstrapper
    {

        public static void Run()
        {
            var httpConfig = new HttpConfiguration();
            SetAutoFacContainer(httpConfig);
            AutoMapperConfiguration.Configure();
        }

        public static IContainer SetAutoFacContainer(HttpConfiguration httpConfig)
        {
            /*var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            //Infrastructure
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();

            //Repositories
            builder.RegisterAssemblyTypes(typeof(DoctorRepository).Assembly)
                .Where(d => d.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            //Services
            builder.RegisterAssemblyTypes(typeof(DoctorService).Assembly)
                .Where(d=>d.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));*/

            var builder = new ContainerBuilder();


            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            //Infrastructure
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();
            builder.Register(c => new UserManager<AllUsers>(new UserStore<AllUsers>(new HmsEntities()))).As<UserManager<AllUsers>>().InstancePerRequest();
            builder.Register(
                c =>
                    new SignInManager<AllUsers, string>(
                        new UserManager<AllUsers, string>(new UserStore<AllUsers>(new HmsEntities())),
                        HttpContext.Current.GetOwinContext().Authentication)).As<SignInManager<AllUsers, string>>().InstancePerRequest();

            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication)
                .As<IAuthenticationManager>();
            builder.RegisterType<RoleStore<IdentityRole>>().As<IRoleStore<IdentityRole, string>>();
            builder.RegisterType<AppRoleManager>();


            //Repositories
            builder.RegisterAssemblyTypes(typeof(DoctorRepository).Assembly)
                .Where(d => d.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            //Services
            builder.RegisterAssemblyTypes(typeof(DoctorService).Assembly)
                .Where(d => d.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterType<BaseApiController>().InstancePerRequest();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            httpConfig.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            return container;


        }
    }
}