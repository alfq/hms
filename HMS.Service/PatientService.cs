﻿using System.Collections.Generic;
using HMS.Data.Infrastructure;
using HMS.Data.Repositories;
using HMS.Model;

namespace HMS.Service
{
    public interface IPatientService
    {
        IEnumerable<Patient> GetPatients();
        //Patient GetPatient(int id);
        void CreatePatient(Patient patient);
        void SavePatient();
        void DeletePatient(Patient patient);
        //void DeletePatient(int id);
        //Patient GetPatientByGeneratedId(string id);
    }

    public class PatientService : IPatientService
    {
        private readonly IPatientRepository _patientsRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PatientService(IPatientRepository patientRepository, IUnitOfWork unitOfWork)
        {
            _patientsRepository = patientRepository;
            _unitOfWork = unitOfWork;
        }


        public IEnumerable<Patient> GetPatients()
        {
            return _patientsRepository.GetAll();
        }

        public Patient GetPatient(int id)
        {
            return _patientsRepository.GetById(id);
        }

        //public Patient GetPatientByGeneratedId(string id)
        //{
        //    var patient = _patientsRepository.Get(p => p.PatientGeneratedId == id);
        //    return patient;
        //}

        public void CreatePatient(Patient patient)
        {
             _patientsRepository.Add(patient);
        }

        public void DeletePatient(Patient patient)
        {
            _patientsRepository.Delete(patient);
        }

        /*public void DeletePatient(int id)
        {
            _patientsRepository.Delete(d=>d.PatientId == id);
        }*/

        public void SavePatient()
        {
            _unitOfWork.Commit();
        }
    }
}