﻿using System.Collections.Generic;
using HMS.Data.Infrastructure;
using HMS.Data.Repositories;
using HMS.Model;

namespace HMS.Service
{
    public interface IDoctorService
    {
        IEnumerable<Doctor> GetDoctors();
        Doctor GetDoctor(int id);
        Doctor GetDoctorById(string id);
        Doctor GetDoctor(string username);
        void CreateDoctor(Doctor doctor);
        void DeleteDoctor(Doctor doctor);
        void UpdateDoctor(Doctor doctor);
        //void DeleteDoctor(int id);
        void SaveDoctor();
    }

    public class DoctorService : IDoctorService
    {
        private readonly IDoctorRepository _doctorsRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DoctorService(IDoctorRepository doctorsRepository,IUnitOfWork unitOfWork)
        {
            _doctorsRepository = doctorsRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Doctor> GetDoctors()
        {
            return _doctorsRepository.GetAll();
        }

        public Doctor GetDoctor(int id)
        {
            var doctor = _doctorsRepository.GetById(id);
            return doctor;
        }

        public void CreateDoctor(Doctor doctor)
        {
            _doctorsRepository.Add(doctor);
        }

        public void DeleteDoctor(Doctor doctor)
        {
            _doctorsRepository.Delete(doctor);
        }

        /*public void DeleteDoctor(int id)
        {
            _doctorsRepository.Delete(d=>d.DoctorId == id);
        }*/

        public void SaveDoctor()
        {
            _unitOfWork.Commit();
        }

        public Doctor GetDoctor(string username)
        {
            return _doctorsRepository.Get(c => c.UserName == username);
        }

        public Doctor GetDoctorById(string id)
        {
            return _doctorsRepository.Get(c=>c.Id == id);
        }

        public void UpdateDoctor(Doctor doctor)
        {
            _doctorsRepository.Update(doctor);
        }
    }
}