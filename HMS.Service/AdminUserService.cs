﻿using System.Collections.Generic;
using HMS.Data.Infrastructure;
using HMS.Data.Repositories;
using HMS.Model;

namespace HMS.Service
{
    public interface IAdminUserService
    {
        void Create(AdminUser adminUser);
        // void Delete(AdminUser adminUser);
        IEnumerable<AdminUser> GetAdminUsers();
        //AdminUser GetAdminUserByPatientGeneratedId(string id);
        void Save();
    }

    public class AdminUserService : IAdminUserService
    {
        private readonly IAdminUserRepository _adminUserRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AdminUserService(IAdminUserRepository adminUserRepository,
            IUnitOfWork unitOfWork)
        {
            _adminUserRepository = adminUserRepository;
            _unitOfWork = unitOfWork;
        }


        public void Create(AdminUser adminUser)
        {
            _adminUserRepository.Add(adminUser);
        }


        public IEnumerable<AdminUser> GetAdminUsers()
        {
            return _adminUserRepository.GetAll();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }

}