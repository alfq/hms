using System.Collections.Generic;
using HMS.Data.Infrastructure;
using HMS.Data.Repositories;
using HMS.Model;

namespace HMS.Service
{

    public interface IBillService
    {
        void Create(Bill bill);
        void Delete(Bill bill);
        IEnumerable<Bill> GetBills();
        //Bill GetBillByPatientGeneratedId(string id);
        void Save();
    }

    public class BillService : IBillService
    {
        private readonly IBillRepository _billRepository;
        private readonly IPatientRepository _patientRepository;
        private readonly IUnitOfWork _unitOfWork;

        public BillService(IBillRepository billRepository, 
            IPatientRepository patientRepository, 
            IUnitOfWork unitOfWork)
        {
            _billRepository = billRepository;
            _patientRepository = patientRepository;
            _unitOfWork = unitOfWork;
        }


        public void Create(Bill bill)
        {
            _billRepository.Add(bill);
        }

        public void Delete(Bill bill)
        {
            _billRepository.Delete(bill);
        }

        public IEnumerable<Bill> GetBills()
        {
            return _billRepository.GetAll();
        }

        /*public Bill GetBillByPatientGeneratedId(string id)
        {
            var patient = _patientRepository.Get(p => p.PatientGeneratedId == id);
            var bill = _billRepository.Get(b => b.Patient == patient);

            return bill;
        }*/

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}