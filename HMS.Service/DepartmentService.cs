using System.Collections.Generic;
using HMS.Data.Infrastructure;
using HMS.Data.Repositories;
using HMS.Model;

namespace HMS.Service
{
    public interface IDepartmentService
    {
        IEnumerable<Department> GetDepartments();
        Department GetDepartmentByName(string name);
        void DeleteDepartmentByName(string name);
        void Create(Department department);
        void Save();
    }

    public class DepartmentService : IDepartmentService
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DepartmentService(IDepartmentRepository departmentRepository, IUnitOfWork unitOfWork)
        {
            _departmentRepository = departmentRepository;
            _unitOfWork = unitOfWork;
        }


        public IEnumerable<Department> GetDepartments()
        {
            return _departmentRepository.GetAll();
        }

        public Department GetDepartmentByName(string name)
        {
            var department = _departmentRepository.Get(d => d.DepartmentName == name);
            return department;
        }

        public void DeleteDepartmentByName(string name)
        {
            _departmentRepository.Delete(c=>c.DepartmentName == name);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Create(Department department)
        {
            _departmentRepository.Add(department);
        }
    }
}