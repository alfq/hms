﻿using System.Collections.Generic;
using HMS.Data.Infrastructure;
using HMS.Data.Repositories;
using HMS.Model;

namespace HMS.Service
{
    public interface IRoomService
    {
        IEnumerable<Room> GetRooms();
        Room GetRoomByName(string name);
        void Create(Room room);
        void DeleteByName(string name);
        void Save();
    }

    public class RoomService : IRoomService
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RoomService(IRoomRepository roomRepository, IUnitOfWork unitOfWork)
        {
            _roomRepository = roomRepository;
            _unitOfWork = unitOfWork;
        }


        public IEnumerable<Room> GetRooms()
        {
            return _roomRepository.GetAll();
        }

        public Room GetRoomByName(string name)
        {
            var room = _roomRepository.Get(r => r.RoomNumber == name);
            return room;
        }

        public void Create(Room room)
        {
            _roomRepository.Add(room);
        }

        public void DeleteByName(string name)
        {
            _roomRepository.Delete(c => c.RoomNumber == name);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}