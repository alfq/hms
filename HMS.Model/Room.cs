﻿namespace HMS.Model
{
    public class Room
    {
        public int RoomId { get; set; }
        public string RoomNumber { get; set; }
        public RoomType RoomType { get; set; }
    }

    public enum RoomType
    {
        General,
        Private
    }
}