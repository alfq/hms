﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HMS.Model
{
    public class Department
    {
        public Department()
        {
            Doctors = new Collection<Doctor>();
        }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public ICollection<Doctor> Doctors { get; set; }
    }
}