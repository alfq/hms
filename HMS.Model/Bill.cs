﻿using System;

namespace HMS.Model
{
    public class Bill
    {
        public int BillId { get; set; }
        public decimal DoctorBillCharge { get; set; }
        public decimal RoomBillCharge { get; set; }
        public decimal TotalCharge { get { return DoctorBillCharge + RoomBillCharge; }
            private set { TotalCharge = value;} }
        public Patient Patient { get; set; }
        public int PatientId { get; set; }
        public DateTime DateOfAdmission { get; set; }
        public DateTime DateOfDischarge { get; set; }
    }
}