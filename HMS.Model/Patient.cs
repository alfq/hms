﻿using System.Numerics;

namespace HMS.Model
{
    public class Patient : AllUsers
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Address { get; set; }
        public Doctor Doctor { get; set; }
        public string DoctorId { get; set; }

        /*
                public string PatientGeneratedId {get { return IdGenerated(); }
                    private set { value = PatientGeneratedId; }
                }
        */

        #region HelperMethods

        /*private string IdGenerated()
        {
            BigInteger i = PatientId;
            var letter = 'A';
            var s = i.ToString().PadLeft(5, '0');
            if (s[1] != '0')
            {
                letter++;
            }
            var generatedId = letter + s;
            return generatedId;
        }*/

        #endregion
    }
}