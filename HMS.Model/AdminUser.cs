﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace HMS.Model
{
    public class AdminUser : AllUsers
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}