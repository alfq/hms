﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HMS.Model
{
    public class Doctor : AllUsers
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Doctor()
        {
            Patients = new Collection<Patient>();
        }

        public string Address { get; set; }
        public Gender Gender { get; set; }
        public ICollection<Patient> Patients { get; set; }
        public Department Department { get; set; }
        public int DepartmentId { get; set; }
    }

    #region Enums
    public enum Gender
    {
        Male,
        Female
    }

    #endregion
}
