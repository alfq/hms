using HMS.Data.Infrastructure;
using HMS.Model;

namespace HMS.Data.Repositories
{
    public interface IRoomRepository : IRepository<Room> { }

    public class RoomRepository : RepositoryBase<Room>, IRoomRepository
    {
        public RoomRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}