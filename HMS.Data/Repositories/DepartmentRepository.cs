using System.Linq;
using HMS.Data.Infrastructure;
using HMS.Model;

namespace HMS.Data.Repositories
{
    public interface IDepartmentRepository : IRepository<Department>
    {
    }

    public class DepartmentRepository: RepositoryBase<Department>,IDepartmentRepository
    {
        public DepartmentRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}