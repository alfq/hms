﻿using HMS.Data.Infrastructure;
using HMS.Model;

namespace HMS.Data.Repositories
{
    public interface IDoctorRepository: IRepository<Doctor> { }

    public class DoctorRepository : RepositoryBase<Doctor>, IDoctorRepository
    {
        public DoctorRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}