using HMS.Data.Infrastructure;
using HMS.Model;

namespace HMS.Data.Repositories
{
    public interface IPatientRepository: IRepository<Patient> { }

    public class PatientRepository : RepositoryBase<Patient>, IPatientRepository
    {
        public PatientRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}