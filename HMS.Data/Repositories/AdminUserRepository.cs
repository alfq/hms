﻿using HMS.Data.Infrastructure;
using HMS.Model;

namespace HMS.Data.Repositories
{
    public interface IAdminUserRepository : IRepository<AdminUser> { }

    public class AdminUserRepository : RepositoryBase<AdminUser>, IAdminUserRepository
    {
        public AdminUserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}