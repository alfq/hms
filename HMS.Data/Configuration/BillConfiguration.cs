using System.Data.Entity.ModelConfiguration;
using HMS.Model;

namespace HMS.Data.Configuration
{
    public class BillConfiguration : EntityTypeConfiguration<Bill>
    {
        public BillConfiguration()
        {
            ToTable("Bills");
            Property(b => b.BillId).IsRequired();
            Property(b => b.DateOfAdmission).IsRequired();
            Property(b => b.DateOfDischarge).IsRequired();
            Property(b => b.DoctorBillCharge).IsRequired().HasPrecision(8, 2);
            Property(b => b.RoomBillCharge).IsRequired().HasPrecision(8, 2);
            Property(b => b.PatientId).IsRequired();
        }
    }
}