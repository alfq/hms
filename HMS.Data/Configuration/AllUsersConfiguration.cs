﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using HMS.Model;

namespace HMS.Data.Configuration
{
    public class AllUsersConfiguration : EntityTypeConfiguration<AllUsers>
    {
        public AllUsersConfiguration()
        {
            Property(c => c.Email)
                .HasMaxLength(50)
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new[] {new IndexAttribute("AU_Doctor_DoctorEmail") {IsUnique = true}}));
            Property(c => c.UserName)
                .HasMaxLength(50)
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("AU_Doctor_DoctorUsername") {IsUnique = true}));

        }
    }
}