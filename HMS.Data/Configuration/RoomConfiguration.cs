using System.Data.Entity.ModelConfiguration;
using HMS.Model;

namespace HMS.Data.Configuration
{
    public class RoomConfiguration : EntityTypeConfiguration<Room>
    {
        public RoomConfiguration()
        {
            ToTable("Rooms");
            Property(r => r.RoomId).IsRequired();
            Property(r => r.RoomNumber).IsRequired();
            Property(r => r.RoomType).IsRequired();
        }
    }
}