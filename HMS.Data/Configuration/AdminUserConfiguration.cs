﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using HMS.Model;

namespace HMS.Data.Configuration
{
    public class AdminUserConfiguration : EntityTypeConfiguration<AdminUser>
    {
        public AdminUserConfiguration()
        {
            ToTable("AdminUsers");
            Property(a=>a.FirstName).IsRequired().HasMaxLength(100);
            Property(a => a.LastName).IsRequired().HasMaxLength(100);
        }
    }
}