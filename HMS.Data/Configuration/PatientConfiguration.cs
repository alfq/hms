using System.Data.Entity.ModelConfiguration;
using HMS.Model;

namespace HMS.Data.Configuration
{
    public class PatientConfiguration : EntityTypeConfiguration<Patient>
    {
        public PatientConfiguration()
        {
            ToTable("Patients");
            Property(d => d.Gender).IsRequired();
            Property(d => d.Address).IsRequired().HasMaxLength(250);
           // Property(d => d.DoctorId).IsRequired();

            //HasOptional(p=>p.Doctor).WithMany(p=>p.Patients).HasForeignKey(p=>p.DoctorId).WillCascadeOnDelete(false);
        }
    }
}