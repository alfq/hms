﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using HMS.Model;

namespace HMS.Data.Configuration
{
    public class DoctorConfiguration : EntityTypeConfiguration<Doctor>
    {
        public DoctorConfiguration()
        {
            ToTable("Doctors");
            Property(d => d.Gender).IsRequired();
            Property(d=>d.Address).IsRequired().HasMaxLength(250);
        }
    }
}