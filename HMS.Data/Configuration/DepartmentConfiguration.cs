using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using HMS.Model;

namespace HMS.Data.Configuration
{
    public class DepartmentConfiguration : EntityTypeConfiguration<Department>
    {
        public DepartmentConfiguration()
        {
            ToTable("Departments");
            Property(d => d.DepartmentId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(d => d.DepartmentName).IsRequired();
            //Property(d => d.DoctorId).IsRequired();

           // HasOptional(d=>d.Doctor).WithMany(d=>d.Departments).HasForeignKey(d=>d.DoctorId).WillCascadeOnDelete(false);
        }
    }
}