﻿using System;
using System.Threading.Tasks;
using HMS.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HMS.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        HmsEntities Init();
    }

    public class DbFactory : Disposable, IDbFactory
    {
        private HmsEntities _dbContext;
        private UserManager<AllUsers> _userManager;

        public DbFactory()
        {
            Init();
            _userManager = new UserManager<AllUsers>(new UserStore<AllUsers>(_dbContext));
        }
        public HmsEntities Init()
        {
            return _dbContext ?? (_dbContext = new HmsEntities());
        }

        public async Task<IdentityResult> RegisterUser(AllUsers userModel)
        {
            var user = new AllUsers
            {
                UserName = userModel.UserName
            };

            var result = await _userManager.CreateAsync(user, userModel.PasswordHash);
            return result;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public async Task<IdentityUser> FindAsync(UserLoginInfo loginInfo)
        {
            IdentityUser user = await _userManager.FindAsync(loginInfo);

            return user;
        }

        public async Task<IdentityResult> CreateAsync(AllUsers user)
        {
            var result = await _userManager.CreateAsync(user);

            return result;
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await _userManager.AddLoginAsync(userId, login);

            return result;
        }

        protected override void DisposeCore()
        {
            _dbContext?.Dispose();
        }
    }

}