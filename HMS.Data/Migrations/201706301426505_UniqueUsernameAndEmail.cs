namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UniqueUsernameAndEmail : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AllUsers", "Email", c => c.String(maxLength: 50));
            AlterColumn("dbo.AllUsers", "UserName", c => c.String(maxLength: 50));
            CreateIndex("dbo.AllUsers", "Email", unique: true, name: "AU_Doctor_DoctorEmail");
            CreateIndex("dbo.AllUsers", "UserName", unique: true, name: "AU_Doctor_DoctorUsername");
        }
        
        public override void Down()
        {
            DropIndex("dbo.AllUsers", "AU_Doctor_DoctorUsername");
            DropIndex("dbo.AllUsers", "AU_Doctor_DoctorEmail");
            AlterColumn("dbo.AllUsers", "UserName", c => c.String());
            AlterColumn("dbo.AllUsers", "Email", c => c.String());
        }
    }
}
