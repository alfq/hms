namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeptDropped : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Doctors", "Department_DepartmentId", "dbo.Departments");
            DropIndex("dbo.Doctors", new[] { "Department_DepartmentId" });
            DropColumn("dbo.Doctors", "Department_DepartmentId");
            DropTable("dbo.Departments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepartmentId = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.DepartmentId);
            
            AddColumn("dbo.Doctors", "Department_DepartmentId", c => c.Int());
            CreateIndex("dbo.Doctors", "Department_DepartmentId");
            AddForeignKey("dbo.Doctors", "Department_DepartmentId", "dbo.Departments", "DepartmentId");
        }
    }
}
