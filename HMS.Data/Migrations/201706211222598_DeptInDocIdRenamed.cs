namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeptInDocIdRenamed : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Doctors", "Department_DepartmentId", "dbo.Departments");
            DropIndex("dbo.Doctors", new[] { "Department_DepartmentId" });
            RenameColumn(table: "dbo.Doctors", name: "Department_DepartmentId", newName: "DepartmentId");
            AlterColumn("dbo.Doctors", "DepartmentId", c => c.Int(nullable: false));
            CreateIndex("dbo.Doctors", "DepartmentId");
            AddForeignKey("dbo.Doctors", "DepartmentId", "dbo.Departments", "DepartmentId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Doctors", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.Doctors", new[] { "DepartmentId" });
            AlterColumn("dbo.Doctors", "DepartmentId", c => c.Int());
            RenameColumn(table: "dbo.Doctors", name: "DepartmentId", newName: "Department_DepartmentId");
            CreateIndex("dbo.Doctors", "Department_DepartmentId");
            AddForeignKey("dbo.Doctors", "Department_DepartmentId", "dbo.Departments", "DepartmentId");
        }
    }
}
