namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BaseClassesModified : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bills", "TotalCharge", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Patients", "PatientGeneratedId", c => c.String());
            DropColumn("dbo.Bills", "BillNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Bills", "BillNumber", c => c.String(nullable: false));
            DropColumn("dbo.Patients", "PatientGeneratedId");
            DropColumn("dbo.Bills", "TotalCharge");
        }
    }
}
