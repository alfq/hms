namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DoctorIdAddedDepartment : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Departments", name: "Doctor_Id", newName: "DoctorId");
            RenameIndex(table: "dbo.Departments", name: "IX_Doctor_Id", newName: "IX_DoctorId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Departments", name: "IX_DoctorId", newName: "IX_Doctor_Id");
            RenameColumn(table: "dbo.Departments", name: "DoctorId", newName: "Doctor_Id");
        }
    }
}
