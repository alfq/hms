using HMS.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HMS.Data.HmsEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HMS.Data.HmsEntities context)
        {
            //  This method will be called after migrating to the latest version.

            var manager = new UserManager<AdminUser>(new UserStore<AdminUser>(new HmsEntities()));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var user = new AdminUser()
            {
                UserName = "AdminUser",
                Email = "adminuser@gmail.com",
                FirstName = "Chuck",
                LastName = "Norris"
            };
            manager.Create(user, "Abc123!");
            if (roleManager.Roles.Count() == 0)
            {
                roleManager.Create(new IdentityRole {Name = "Admin"});
                roleManager.Create(new IdentityRole {Name = "Doctor"});
                roleManager.Create(new IdentityRole {Name = "Patient"});
            }

            var adminUser = manager.FindByEmail("adminuser@gmail.com");
            manager.AddToRoles(adminUser.Id, new[] {"Admin","Doctor","Patient"});
        }
    }
}
