namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OneToManyRelationshipDefined : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Patients", "DoctorId", "dbo.Doctors");
            DropForeignKey("dbo.Departments", "DoctorId", "dbo.Doctors");
            AddForeignKey("dbo.Patients", "DoctorId", "dbo.Doctors", "DoctorId");
            AddForeignKey("dbo.Departments", "DoctorId", "dbo.Doctors", "DoctorId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Departments", "DoctorId", "dbo.Doctors");
            DropForeignKey("dbo.Patients", "DoctorId", "dbo.Doctors");
            AddForeignKey("dbo.Departments", "DoctorId", "dbo.Doctors", "DoctorId", cascadeDelete: true);
            AddForeignKey("dbo.Patients", "DoctorId", "dbo.Doctors", "DoctorId", cascadeDelete: true);
        }
    }
}
