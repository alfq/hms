namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PatientChanged : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Bills", "PatientId", "dbo.Patients");
            DropIndex("dbo.Bills", new[] { "PatientId" });
            DropPrimaryKey("dbo.Patients");
            AddColumn("dbo.Bills", "Patient_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Patients", "Id", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Patients", "Id");
            CreateIndex("dbo.Bills", "Patient_Id");
            CreateIndex("dbo.Patients", "Id");
            AddForeignKey("dbo.Patients", "Id", "dbo.AllUsers", "Id");
            AddForeignKey("dbo.Bills", "Patient_Id", "dbo.Patients", "Id");
            DropColumn("dbo.Patients", "PatientId");
            DropColumn("dbo.Patients", "FirstName");
            DropColumn("dbo.Patients", "LastName");
            DropColumn("dbo.Patients", "PhoneNumber");
            DropColumn("dbo.Patients", "PatientGeneratedId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Patients", "PatientGeneratedId", c => c.String());
            AddColumn("dbo.Patients", "PhoneNumber", c => c.String(nullable: false));
            AddColumn("dbo.Patients", "LastName", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Patients", "FirstName", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Patients", "PatientId", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Bills", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.Patients", "Id", "dbo.AllUsers");
            DropIndex("dbo.Patients", new[] { "Id" });
            DropIndex("dbo.Bills", new[] { "Patient_Id" });
            DropPrimaryKey("dbo.Patients");
            DropColumn("dbo.Patients", "Id");
            DropColumn("dbo.Bills", "Patient_Id");
            AddPrimaryKey("dbo.Patients", "PatientId");
            CreateIndex("dbo.Bills", "PatientId");
            AddForeignKey("dbo.Bills", "PatientId", "dbo.Patients", "PatientId", cascadeDelete: true);
        }
    }
}
