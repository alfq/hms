namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DocRemovedFromDept : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Departments", "DoctorId", "dbo.Doctors");
            DropForeignKey("dbo.Doctors", "Department_DepartmentId", "dbo.Departments");
            DropIndex("dbo.Departments", new[] { "DoctorId" });
            DropIndex("dbo.Doctors", new[] { "Department_DepartmentId" });
            DropColumn("dbo.Doctors", "Department_DepartmentId");
            DropColumn("dbo.Departments", "DoctorId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Departments", "DoctorId", c => c.String(maxLength: 128));
            AddColumn("dbo.Doctors", "Department_DepartmentId", c => c.Int());
            CreateIndex("dbo.Doctors", "Department_DepartmentId");
            CreateIndex("dbo.Departments", "DoctorId");
            AddForeignKey("dbo.Doctors", "Department_DepartmentId", "dbo.Departments", "DepartmentId");
            AddForeignKey("dbo.Departments", "DoctorId", "dbo.Doctors", "Id");
        }
    }
}
