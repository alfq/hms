namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeptAdded : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Departments", "DepartmentName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Departments", "DepartmentName", c => c.String(nullable: false));
        }
    }
}
