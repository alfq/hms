namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DoctorAddedToAllUsers : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Departments", "DoctorId", "dbo.Doctors");
            DropForeignKey("dbo.Patients", "DoctorId", "dbo.Doctors");
            DropIndex("dbo.Departments", new[] { "DoctorId" });
            DropIndex("dbo.Patients", new[] { "DoctorId" });
            RenameColumn(table: "dbo.Patients", name: "DoctorId", newName: "Doctor_Id");
            RenameColumn(table: "dbo.Departments", name: "DoctorId", newName: "Doctor_Id");
            DropPrimaryKey("dbo.Doctors");
            AddColumn("dbo.Doctors", "Id", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Patients", "Doctor_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.Departments", "Doctor_Id", c => c.String(maxLength: 128));
            AddPrimaryKey("dbo.Doctors", "Id");
            CreateIndex("dbo.Departments", "Doctor_Id");
            CreateIndex("dbo.Doctors", "Id");
            CreateIndex("dbo.Patients", "Doctor_Id");
            AddForeignKey("dbo.Doctors", "Id", "dbo.AllUsers", "Id");
            AddForeignKey("dbo.Departments", "Doctor_Id", "dbo.Doctors", "Id");
            AddForeignKey("dbo.Patients", "Doctor_Id", "dbo.Doctors", "Id");
            DropColumn("dbo.Doctors", "DoctorId");
            DropColumn("dbo.Doctors", "FirstName");
            DropColumn("dbo.Doctors", "LastName");
            DropColumn("dbo.Doctors", "PhoneNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Doctors", "PhoneNumber", c => c.String(nullable: false));
            AddColumn("dbo.Doctors", "LastName", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Doctors", "FirstName", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Doctors", "DoctorId", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Patients", "Doctor_Id", "dbo.Doctors");
            DropForeignKey("dbo.Departments", "Doctor_Id", "dbo.Doctors");
            DropForeignKey("dbo.Doctors", "Id", "dbo.AllUsers");
            DropIndex("dbo.Patients", new[] { "Doctor_Id" });
            DropIndex("dbo.Doctors", new[] { "Id" });
            DropIndex("dbo.Departments", new[] { "Doctor_Id" });
            DropPrimaryKey("dbo.Doctors");
            AlterColumn("dbo.Departments", "Doctor_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Patients", "Doctor_Id", c => c.Int(nullable: false));
            DropColumn("dbo.Doctors", "Id");
            AddPrimaryKey("dbo.Doctors", "DoctorId");
            RenameColumn(table: "dbo.Departments", name: "Doctor_Id", newName: "DoctorId");
            RenameColumn(table: "dbo.Patients", name: "Doctor_Id", newName: "DoctorId");
            CreateIndex("dbo.Patients", "DoctorId");
            CreateIndex("dbo.Departments", "DoctorId");
            AddForeignKey("dbo.Patients", "DoctorId", "dbo.Doctors", "DoctorId");
            AddForeignKey("dbo.Departments", "DoctorId", "dbo.Doctors", "DoctorId");
        }
    }
}
