namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            Sql("DBCC CHECKIDENT (Patients, RESEED,0)");
        }
        
        public override void Down()
        {
        }
    }
}
