namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DoctorIdAddedToPatient : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Patients", name: "Doctor_Id", newName: "DoctorId");
            RenameIndex(table: "dbo.Patients", name: "IX_Doctor_Id", newName: "IX_DoctorId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Patients", name: "IX_DoctorId", newName: "IX_Doctor_Id");
            RenameColumn(table: "dbo.Patients", name: "DoctorId", newName: "Doctor_Id");
        }
    }
}
