namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstLastNameMoved : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdminUsers", "FirstName", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.AdminUsers", "LastName", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.Doctors", "FirstName", c => c.String());
            AddColumn("dbo.Doctors", "LastName", c => c.String());
            AddColumn("dbo.Patients", "FirstName", c => c.String());
            AddColumn("dbo.Patients", "LastName", c => c.String());
            DropColumn("dbo.AllUsers", "FirstName");
            DropColumn("dbo.AllUsers", "LastName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AllUsers", "LastName", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.AllUsers", "FirstName", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.Patients", "LastName");
            DropColumn("dbo.Patients", "FirstName");
            DropColumn("dbo.Doctors", "LastName");
            DropColumn("dbo.Doctors", "FirstName");
            DropColumn("dbo.AdminUsers", "LastName");
            DropColumn("dbo.AdminUsers", "FirstName");
        }
    }
}
