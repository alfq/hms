namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AllUsersTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.AspNetUserClaims", name: "AdminUser_Id", newName: "AllUsers_Id");
            RenameColumn(table: "dbo.AspNetUserLogins", name: "AdminUser_Id", newName: "AllUsers_Id");
            RenameColumn(table: "dbo.AspNetUserRoles", name: "AdminUser_Id", newName: "AllUsers_Id");
            RenameIndex(table: "dbo.AspNetUserClaims", name: "IX_AdminUser_Id", newName: "IX_AllUsers_Id");
            RenameIndex(table: "dbo.AspNetUserLogins", name: "IX_AdminUser_Id", newName: "IX_AllUsers_Id");
            RenameIndex(table: "dbo.AspNetUserRoles", name: "IX_AdminUser_Id", newName: "IX_AllUsers_Id");
            CreateTable(
                "dbo.AllUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.AdminUsers", "Id");
            AddForeignKey("dbo.AdminUsers", "Id", "dbo.AllUsers", "Id");
            DropColumn("dbo.AdminUsers", "FirstName");
            DropColumn("dbo.AdminUsers", "LastName");
            DropColumn("dbo.AdminUsers", "Email");
            DropColumn("dbo.AdminUsers", "EmailConfirmed");
            DropColumn("dbo.AdminUsers", "PasswordHash");
            DropColumn("dbo.AdminUsers", "SecurityStamp");
            DropColumn("dbo.AdminUsers", "PhoneNumber");
            DropColumn("dbo.AdminUsers", "PhoneNumberConfirmed");
            DropColumn("dbo.AdminUsers", "TwoFactorEnabled");
            DropColumn("dbo.AdminUsers", "LockoutEndDateUtc");
            DropColumn("dbo.AdminUsers", "LockoutEnabled");
            DropColumn("dbo.AdminUsers", "AccessFailedCount");
            DropColumn("dbo.AdminUsers", "UserName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AdminUsers", "UserName", c => c.String());
            AddColumn("dbo.AdminUsers", "AccessFailedCount", c => c.Int(nullable: false));
            AddColumn("dbo.AdminUsers", "LockoutEnabled", c => c.Boolean(nullable: false));
            AddColumn("dbo.AdminUsers", "LockoutEndDateUtc", c => c.DateTime());
            AddColumn("dbo.AdminUsers", "TwoFactorEnabled", c => c.Boolean(nullable: false));
            AddColumn("dbo.AdminUsers", "PhoneNumberConfirmed", c => c.Boolean(nullable: false));
            AddColumn("dbo.AdminUsers", "PhoneNumber", c => c.String());
            AddColumn("dbo.AdminUsers", "SecurityStamp", c => c.String());
            AddColumn("dbo.AdminUsers", "PasswordHash", c => c.String());
            AddColumn("dbo.AdminUsers", "EmailConfirmed", c => c.Boolean(nullable: false));
            AddColumn("dbo.AdminUsers", "Email", c => c.String());
            AddColumn("dbo.AdminUsers", "LastName", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.AdminUsers", "FirstName", c => c.String(nullable: false, maxLength: 100));
            DropForeignKey("dbo.AdminUsers", "Id", "dbo.AllUsers");
            DropIndex("dbo.AdminUsers", new[] { "Id" });
            DropTable("dbo.AllUsers");
            RenameIndex(table: "dbo.AspNetUserRoles", name: "IX_AllUsers_Id", newName: "IX_AdminUser_Id");
            RenameIndex(table: "dbo.AspNetUserLogins", name: "IX_AllUsers_Id", newName: "IX_AdminUser_Id");
            RenameIndex(table: "dbo.AspNetUserClaims", name: "IX_AllUsers_Id", newName: "IX_AdminUser_Id");
            RenameColumn(table: "dbo.AspNetUserRoles", name: "AllUsers_Id", newName: "AdminUser_Id");
            RenameColumn(table: "dbo.AspNetUserLogins", name: "AllUsers_Id", newName: "AdminUser_Id");
            RenameColumn(table: "dbo.AspNetUserClaims", name: "AllUsers_Id", newName: "AdminUser_Id");
        }
    }
}
