﻿using System.Data.Entity;
using HMS.Data.Configuration;
using HMS.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HMS.Data
{
    public class HmsEntities : IdentityDbContext<AdminUser>
    {
        public HmsEntities():base("HmsEntities") {}

        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Bill> Bills { get; set; }
        public DbSet<AllUsers> AllUsers { get; set; }

        public void Commit()
        {
            SaveChanges();
        }        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new DoctorConfiguration());
            modelBuilder.Configurations.Add(new PatientConfiguration());
            modelBuilder.Configurations.Add(new RoomConfiguration());
            modelBuilder.Configurations.Add(new BillConfiguration());
            modelBuilder.Configurations.Add(new DepartmentConfiguration());
            modelBuilder.Configurations.Add(new AdminUserConfiguration());
            modelBuilder.Configurations.Add(new AllUsersConfiguration());
        }
    }

}