﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HMS.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HMS.Tests.Helpers
{
    public class TestUserStore : IUserStore<AllUsers>, IUserLoginStore<AllUsers>, IUserRoleStore<AllUsers>, IUserClaimStore<AllUsers>, IUserPasswordStore<AllUsers>, IUserSecurityStampStore<AllUsers>
    {
        private Dictionary<string, AllUsers> _users;
        private Dictionary<IdentityUserLogin, AllUsers> _logins = new Dictionary<IdentityUserLogin, AllUsers>();


        public TestUserStore()
        {
            _users = new Dictionary<string, AllUsers>()
            {
                { "TestUser1", new AllUsers { UserName="Sharon", Id="15e8cda1-3ea3-473d-902f-b62aa55816db"}},
                { "TestUser2", new AllUsers { UserName="adarsh", Id="67e6yda1-3ea3-473d-902f-b62er55816db"}},
                { "TestUser3", new AllUsers { UserName="Shiju",  Id="78e5fda1-3ea3-473d-902f-b62aa86716db"}}
            };

        }


        public Task CreateAsync(AllUsers user, string password)
        {
            _users[user.Id] = user;
            return Task.FromResult(0);
        }

        public Task CreateAsync(AllUsers user)
        {
            _users[user.Id] = user;
            return Task.FromResult(0);
        }
        public Task UpdateAsync(AllUsers user)
        {
            _users[user.Id] = user;
            return Task.FromResult(0);
        }

        public Task<AllUsers> FindByIdAsync(string userId)
        {
            if (_users.ContainsKey(userId))
            {
                return Task.FromResult(_users[userId]);
            }
            return Task.FromResult<AllUsers>(null);
        }

        public void Dispose()
        {
        }

        public IQueryable<AllUsers> Users
        {
            get
            {
                return _users.Values.AsQueryable();
            }
        }
        public Task<AllUsers> FindAsync(string userName, string password)
        {
            var user = new AllUsers() { UserName = "adarsh", Id = "402bd590-fdc7-49ad-9728-40efbfe512ec", PasswordHash = "abcd" };
            return Task.FromResult(user);
        }
        public Task<AllUsers> FindByNameAsync(string userName)
        {

            foreach (AllUsers user in _users.Values)
            {
                if (user.UserName == userName)
                    return Task.FromResult(user);
            }
            return Task.FromResult<AllUsers>(null);
        }

        public Task AddLoginAsync(AllUsers user, IdentityUserLogin login)
        {
            user.Logins.Add(login);
            _logins[login] = user;
            return Task.FromResult(0);
        }

        public Task AddLoginAsync(AllUsers user, UserLoginInfo login)
        {
            throw new NotImplementedException();
        }
        public Task RemoveLoginAsync(AllUsers user, IdentityUserLogin login)
        {
            var logs = user.Logins.Where(l => l.ProviderKey == login.ProviderKey && l.LoginProvider == login.LoginProvider).ToList();
            foreach (var l in logs)
            {
                user.Logins.Remove(l);
                _logins[l] = null;
            }
            return Task.FromResult(0);
        }

        public Task RemoveLoginAsync(AllUsers user, UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(AllUsers user)
        {
            throw new NotImplementedException();
        }

        public Task<AllUsers> FindAsync(IdentityUserLogin login)
        {
            if (_logins.ContainsKey(login))
            {
                return Task.FromResult(_logins[login]);
            }
            return Task.FromResult<AllUsers>(null);
        }


        public Task<AllUsers> FindAsync(UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        public Task AddToRoleAsync(AllUsers user, IdentityUserRole role)
        {
            user.Roles.Add(role);
            return Task.FromResult(0);
        }

        public Task AddToRoleAsync(AllUsers user, string role)
        {
            throw new NotImplementedException();
        }
        public Task RemoveFromRoleAsync(AllUsers user, IdentityUserRole role)
        {
            user.Roles.Remove(role);
            return Task.FromResult(0);
        }

        public Task RemoveFromRoleAsync(AllUsers user, string role)
        {
            throw new NotImplementedException();
        }

        public Task<IList<string>> GetRolesAsync(AllUsers user)
        {
            return Task.FromResult<IList<string>>(new List<string>());
        }

        public Task<bool> IsInRoleAsync(AllUsers user, string role)
        {
            return Task.FromResult<bool>(true);
        }

        public Task<IList<Claim>> GetClaimsAsync(AllUsers user)
        {
            return Task.FromResult<IList<Claim>>(new List<Claim>());
        }

        public Task AddClaimAsync(AllUsers user, IdentityUserClaim claim)
        {
            user.Claims.Add(claim);
            return Task.FromResult(0);
        }

        public Task AddClaimAsync(AllUsers user, Claim claim)
        {
            throw new NotImplementedException();
        }

        public Task RemoveClaimAsync(AllUsers user, IdentityUserClaim claim)
        {
            user.Claims.Remove(claim);
            return Task.FromResult(0);
        }

        public Task RemoveClaimAsync(AllUsers user, Claim claim)
        {

            throw new NotImplementedException();
        }

        public Task SetPasswordHashAsync(AllUsers user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task<string> GetPasswordHashAsync(AllUsers user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task SetSecurityStampAsync(AllUsers user, string stamp)
        {
            user.SecurityStamp = stamp;
            return Task.FromResult(0);
        }

        public Task<string> GetSecurityStampAsync(AllUsers user)
        {
            return Task.FromResult(user.SecurityStamp);
        }

        public Task DeleteAsync(AllUsers user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> HasPasswordAsync(AllUsers user)
        {
            return Task.FromResult(user.PasswordHash != null);
        }
    }
}
