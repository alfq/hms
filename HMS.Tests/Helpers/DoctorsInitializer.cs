using System.Collections.Generic;
using HMS.Model;

namespace HMS.Tests.Helpers
{
    public class DoctorsInitializer
    {
        public static List<Doctor> GetAllDoctors()
        {
            var doctors = new List<Doctor>
            {
                new Doctor()
                {
                    Id = "1",
                    FirstName = "Ewurabena",
                    LastName = "Adams",
                    Email = "ewuradr@gmail.com",
                    Gender = Gender.Female,
                    Address = "Accra,Ghana",
                    DepartmentId = 1
                },
                new Doctor()
                {
                    Id = "2",
                    FirstName = "Jemima",
                    Email = "umoh@gmail.com",
                    LastName = "Umoh",
                    Gender = Gender.Female,
                    Address = "Lagos,Nigeria",
                    DepartmentId = 2
                },
                new Doctor()
                {
                    Id = "3",
                    FirstName = "Patience",
                    LastName = "Akosua",
                    Email = "akosua@gmail.com",
                    Gender = Gender.Female,
                    Address = "Ho,Ghana",
                    DepartmentId = 3
                },
                new Doctor()
                {
                    Id = "4",
                    FirstName = "Akwetey",
                    LastName = "Papafio",
                    Email = "papafio@gmail.com",
                    Gender = Gender.Male,
                    Address = "Accra,Ghana",
                    DepartmentId = 3
                }
            };
            return doctors;
        }
    }
}