﻿using System;
using System.Collections.Generic;
using System.Linq;
using HMS.Data.Infrastructure;
using HMS.Data.Repositories;
using HMS.Model;
using HMS.Service;
using HMS.Tests.Helpers;
using Moq;
using NUnit.Framework;


namespace HMS.Tests.Services
{
    [TestFixture]
    public class ServicesTests
    {
        private IDoctorService _doctorService;
        private IDoctorRepository _doctorRepository;
        private IUnitOfWork _unitOfWork;
        private List<Doctor> _randomDoctors;

        #region Setup

        
            
        [SetUp]
        public void Setup()
        {
            _randomDoctors = SetupDoctors();
            _doctorRepository = SetupDoctorsRepository();
            _unitOfWork = new Mock<IUnitOfWork>().Object;
            _doctorService = new DoctorService(_doctorRepository,_unitOfWork);
            
        }

        private IDoctorRepository SetupDoctorsRepository()
        {
            var repo = new Mock<IDoctorRepository>();

            //Setup mocking behaviour
            repo.Setup(r =>r.GetAll()).Returns(_randomDoctors);

            repo.Setup(r => r.GetById((int) It.IsAny<object>()))
                .Returns(new Func<string, Doctor>(
                    id => _randomDoctors.Find(d => d.Id == id)));

            repo.Setup(r => r.Add(It.IsAny<Doctor>()))
                .Callback(new Action<Doctor>(newDoctor =>
                {
                    newDoctor.Id = new Guid("D").ToString();
                    newDoctor.Email = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 9) +
                                      "@gmail.com";
                    newDoctor.UserName = newDoctor.Email;
                    newDoctor.Gender = Gender.Female;
                    newDoctor.DepartmentId = new Random().Next(1, 4);
                }));

            repo.Setup(r => r.Update(It.IsAny<Doctor>()))
                .Callback(new Action<Doctor>(x =>
                {
                    var oldDoc = _randomDoctors.Find(a => a.Email == x.Email);
                    oldDoc.DepartmentId = x.DepartmentId;
                    oldDoc.Gender = x.Gender;
                    oldDoc.FirstName = x.FirstName;
                    oldDoc.LastName = x.LastName;
                    oldDoc.Address = x.Address;
                    oldDoc.Email = x.Email;
                }));

            repo.Setup(r => r.Delete(It.IsAny<Doctor>()))
                .Callback(new Action<Doctor>(x =>
                {
                    var docToRemove = _randomDoctors.Find(c => c.Email == x.Email);
                    if (docToRemove != null)
                    {
                        _randomDoctors.Remove(docToRemove);
                    }
                }));
            return repo.Object;

        }

        private List<Doctor> SetupDoctors()
        {
            var doctors = DoctorsInitializer.GetAllDoctors();
            return doctors;
        }
        #endregion

        [Test]
        public void ServiceShouldReturnAllDoctors()
        {
            var docs = _doctorService.GetDoctors();
            Assert.That(docs,Is.EqualTo(_randomDoctors));
        }

        [Test]
        public void ServiceShouldReturnRightDoctor()
        {
            var doc = _doctorService.GetDoctors().Single(c => c.Id == "1");
            Assert.That(doc,Is.EqualTo(_randomDoctors.Find(c=>c.Id == "1")));
        }

        [Test]
        public void ServiceShouldAddNewDoctor()
        {
            var doc = new Doctor
            {
                Id = "5",
                FirstName = "James",
                LastName = "Koomson",
                Email = "james@gmail.com",
                Gender = Gender.Male,
                Address = "Accra,Ghana",
                DepartmentId = 1
            };
            var count = _randomDoctors.Count;
            _randomDoctors.Add(doc);
            Assert.That(_randomDoctors.Count,Is.EqualTo(count+1));
        }

        [Test]
        public void ServiceShouldUpdateFirstDoctorName()
        {
            var doc = _randomDoctors.First();
            if (doc != null)
            {
                doc.Email = "ewurabena@gmail.com";
                _doctorService.UpdateDoctor(doc);
            }
            Assert.That(_randomDoctors.First().Email,Is.EqualTo("ewurabena@gmail.com"));

        }

        [Test]
        public void ServiceShouldDeleteDoctor()
        {
            var doc = _randomDoctors.First(c => c.Email == "ewuradr@gmail.com");
            if (doc == null) return;
            _doctorService.DeleteDoctor(doc);
            Assert.That(_randomDoctors.Count,Is.EqualTo(3));
        }
    }
}