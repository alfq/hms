import { HmsPage } from './app.po';

describe('hms App', () => {
  let page: HmsPage;

  beforeEach(() => {
    page = new HmsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
