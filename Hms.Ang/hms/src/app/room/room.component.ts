import { RoomDataService } from 'app/shared/services/room-data.service';
import { ConfigService } from 'app/shared/utils/config.service';
import { DataService } from './../shared/services/data.service';
import { ItemsService } from './../utils/items.service';
import { IRoom, RoomType } from './../shared/interfaces';
import { Component, OnInit, Input } from '@angular/core';
import { NotificationsService } from "angular2-notifications";


@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {

constructor(private _service: NotificationsService){
    
  }

  ngOnInit() {
   // this._service.success("Heya","Created!")
  }

}
