import { GetRoomsComponent } from './../room.components/get-rooms/get-rooms.component';
import { CreateRoomComponent } from './../room.components/create-room/create-room.component';
import { Routes } from "@angular/router";

import { RoomComponent } from "../room.component";

export const RoomRoutes: Routes = [
    {
        path: "rooms",
        component: RoomComponent,
        children: [
            {path: "getrooms", component: GetRoomsComponent},
            {path: "create", component: CreateRoomComponent}
        ]
    }
]