import { IRoom, RoomType } from './../../../shared/interfaces';
import { ItemsService } from './../../../utils/items.service';
import { RoomDataService } from 'app/shared/services/room-data.service';
import { ConfigService } from 'app/shared/utils/config.service';

import { Component, OnInit, Input } from '@angular/core';


@Component({
    templateUrl:"./create-room.component.html"
})

export class CreateRoomComponent implements OnInit {

  rooms: Room[];
  room = new Room;



  constructor(private itemsService: ItemsService, 
    private roomDataService: RoomDataService,
    private configService: ConfigService) { }

  ngOnInit() {
    
  }

createRoom():void{
  console.log(this.room);
  // this.roomDataService.createRoom(this.room)
  //   .subscribe();
}


}

export class Room implements IRoom{
  RoomNumber: number;
  RoomType: RoomType;
}