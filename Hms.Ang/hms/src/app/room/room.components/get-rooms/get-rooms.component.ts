import { IRoom, RoomType } from './../../../shared/interfaces';
import { ConfigService } from './../../../shared/utils/config.service';
import { RoomDataService } from './../../../shared/services/room-data.service';
import { ItemsService } from './../../../utils/items.service';
import { Component, OnInit } from '@angular/core';
import { Modal } from "angular2-modal/plugins/bootstrap";

@Component({
    templateUrl:"./get-rooms.component.html",
    styleUrls:["./get-rooms.component.css"]
})
export class GetRoomsComponent implements OnInit{

rooms: Room[];
  room = new Room;


  constructor(private itemsService: ItemsService, 
    private roomDataService: RoomDataService,
    private configService: ConfigService,
    private modal: Modal) { }

  ngOnInit() {
    
  }

getRooms(): void{
  this.roomDataService.getRooms()
  .subscribe(rooms => this.rooms = rooms);
}


deleteRoom(room){
   this.roomDataService.deleteRoom(room.roomNumber).subscribe((res)=>{
     this.itemsService.removeItemFromArray<Room>(this.rooms,room);
   });
}

onDelete(room) {
this.modal.confirm()
        .size('sm')
        .showClose(true)
        .title('Are you sure you want to delete this room?')
        .okBtn("Yes")
        .cancelBtn("NO")
        .okBtnClass("btn btn-danger")
        .open().then(res => {
          return res.result.then(
          ans => this.dialogResult(ans,room), ()=>{}
        )})
}

dialogResult(result: boolean,room){
  if (result) {
    this.deleteRoom(room)
  }
}
}


export class Room implements IRoom{
  RoomNumber: number;
  RoomType: RoomType;

}