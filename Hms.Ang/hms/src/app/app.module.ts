import { GetRoomsComponent } from './room/room.components/get-rooms/get-rooms.component';
import { CreateRoomComponent } from './room/room.components/create-room/create-room.component';
import { ItemsService } from './utils/items.service';
import { DateFormatPipe } from './shared/pipes/date-format.pipe';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routing } from "./routes";
import { FormsModule } from "@angular/forms";
import "./rxjs-operators";

import { AppComponent } from './app.component';
import { RoomComponent } from './room/room.component';
import { DoctorComponent } from './doctor/doctor.component';
import { DepartmentComponent } from './department/department.component';
import { PatientComponent } from './patient/patient.component';
import { AdminComponent } from './admin/admin.component';
import { DataService } from "app/shared/services/data.service";
import { ConfigService } from "app/shared/utils/config.service";
import { HttpModule } from "@angular/http";
import { RoomDataService } from "app/shared/services/room-data.service";
import { SimpleNotificationsModule } from "angular2-notifications";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { ModalModule } from "angular2-modal";
import { BootstrapModalModule } from "angular2-modal/plugins/bootstrap";

@NgModule({
  declarations: [
    AppComponent,
    RoomComponent,
    DoctorComponent,
    DepartmentComponent,
    PatientComponent,
    AdminComponent,
    DateFormatPipe,
    CreateRoomComponent,
    GetRoomsComponent
  ],
  imports: [
    BrowserModule,
    Routing,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    SimpleNotificationsModule.forRoot()
  ],
  providers: [DataService,
            ConfigService,
             ItemsService,
            RoomDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
