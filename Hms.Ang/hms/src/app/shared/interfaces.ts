export interface IRoom {
    RoomNumber: number,
    RoomType: RoomType
}

export class Doctor implements IAllUsers{
    id:number;
    firstname:string;
    lastname:string;
    address:string;
    gender: Gender;
    patients: Patient[];
    deparments:IDepartment[];
}


export class Patient implements IAllUsers{
    id:number;
    firstname: string;
    lastname: string;
    address: string;
    gender: Gender;
    doctor: Doctor
}

export interface IAllUsers{
    id:number,
    firstname:string,
    lastname:string,
    address:string,
}

export interface IDepartment{
    name:string,
    doctors: Doctor[]
}

export enum Gender{
    Male,
    Female
}

export enum RoomType{
    General,
    Private
}

export interface Predicate<T>{
    (item: T):boolean
}