import { IRoom } from './../interfaces';
import { ConfigService } from 'app/shared/utils/config.service';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";

import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()
export class RoomDataService{
    _baseUrl: string = "http://localhost:55449/api/"

    constructor(private http: Http, private configService: ConfigService){
        //this._baseUrl = configService.getApiURI();
    }

    getRooms(): Observable<IRoom[]>{
        return this.http.get(this._baseUrl +"rooms/getallrooms")
            .map((res:Response)=>{
                return res.json();
            })
            .catch(this.handleError);
    }

    createRoom(room: IRoom): Observable<void> {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");

        return this.http.post(this._baseUrl + "rooms/create", JSON.stringify(room),{
            headers: headers
        }).map((res:Response)=>{
            return res.json();
        }).catch(this.handleError);
    }

    deleteRoom(roomNumber: string):Observable<void>{
        
        return this.http.delete(this._baseUrl + "rooms/delete/"+ roomNumber)
        .map((res:Response)=>{
            return;
        }).catch(this.handleError);
    }

    private handleError(error:any) {
        var applicationError = error.headers.get("Application-Error");
        var serverError = error.json();
        var modelStateErrors:string = "";

        if (!serverError.type) {
            console.log(serverError);
            for(var key in serverError){
                if (serverError[key]) {
                    modelStateErrors +=serverError[key] + "\n";
                }
            }
            modelStateErrors = modelStateErrors = "" ? null:modelStateErrors;
            return Observable.throw(applicationError || modelStateErrors || "Server Error");
        }
    }
}