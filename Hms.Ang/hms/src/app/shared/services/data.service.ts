import { IAllUsers } from './../interfaces';
import { ConfigService } from './../utils/config.service';
import { Injectable } from "@angular/core";
import { Http,Response,Headers } from "@angular/http";

import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()
export class DataService{
    _baseUrl: string = "";

    constructor(private http: Http,private configService:ConfigService){

            this._baseUrl = configService.getApiURI();
        }

    getUsers(userType: string): Observable<IAllUsers[]> {
        return this.http.get(this._baseUrl+ userType + "/")
            .map((res:Response)=>{
                return res.json();
            })
            .catch(this.handleError);
    }

    createUsers(userType:string, user:IAllUsers) : Observable<IAllUsers>{
        let headers = new Headers();
        headers.append("Content-Type","application/json");

        return this.http.post(this._baseUrl+ userType + "/",JSON.stringify(user),{
            headers:headers
        }).map((res:Response) =>{
            return res.json();
        }).catch(this.handleError);
    }

    updateUser(userType:string, user:IAllUsers):Observable<void>{
        let headers = new Headers();
        headers.append("Content-type","application/json");

        return this.http.put(this._baseUrl+ userType + "/" + user.id,JSON.stringify(user),{
            headers:headers
        })
        .map((res:Response)=>{
            return;
        }).catch(this.handleError);
    }

    deleteUser(userType:string, id:number): Observable<void>{
        return this.http.delete(this._baseUrl+userType+"/"+ id)
            .map((res:Response)=>{
                return;
            }).catch(this.handleError);
    }

    private handleError(error:any) {
        var applicationError = error.headers.get("Application-Error");
        var serverError = error.json();
        var modelStateErrors:string = "";

        if (!serverError.type) {
            console.log(serverError);
            for(var key in serverError){
                if (serverError[key]) {
                    modelStateErrors +=serverError[key] + "\n";
                }
            }
            modelStateErrors = modelStateErrors = "" ? null:modelStateErrors;
            return Observable.throw(applicationError || modelStateErrors || "Server Error");
        }
    }
}