import { RoomRoutes } from './room/room.routes/room.routes';
import { AdminComponent } from './admin/admin.component';
import { PatientComponent } from './patient/patient.component';
import { DoctorComponent } from './doctor/doctor.component';
import { DepartmentComponent } from './department/department.component';
import { RoomComponent } from './room/room.component';
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";


const appRoutes:Routes =[
    {path: "rooms", component: RoomComponent},
    {path: "departments", component: DepartmentComponent},
    {path: "doctors", component: DoctorComponent},
    {path: "patients", component: PatientComponent},
    {path: "admin", component: AdminComponent},
    ...RoomRoutes
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);